BEGIN;

DROP TABLE IF EXISTS accounts;
CREATE TABLE accounts
(
    account_id bigserial,
    name character varying(50),
    credit bigint CHECK (credit >= 0),
    PRIMARY KEY (account_id)
);

INSERT INTO accounts(name, credit) VALUES ('Jeffery Ford', 1000);
INSERT INTO accounts(name, credit) VALUES ('Matthew Miller', 1000);
INSERT INTO accounts(name, credit) VALUES ('Michelle Ross', 1000);

COMMIT;

BEGIN;
	SAVEPOINT SP1;
	UPDATE accounts SET credit = credit + 500 WHERE account_id = 3;
	UPDATE accounts SET credit = credit - 500 WHERE account_id = 1;
	
	SAVEPOINT SP2;
	UPDATE accounts SET credit = credit + 700 WHERE account_id = 1;
	UPDATE accounts SET credit = credit - 700 WHERE account_id = 2;

	SAVEPOINT SP3;
	UPDATE accounts SET credit = credit + 100 WHERE account_id = 3;
	UPDATE accounts SET credit = credit - 100 WHERE account_id = 2;
	
	ROLLBACK TO SAVEPOINT SP2;
ROLLBACK;