BEGIN;

DROP TABLE IF EXISTS accounts;
CREATE TABLE accounts
(
    account_id bigserial,
    name character varying(50),
    credit bigint CHECK (credit >= 0),
	bank_name character varying(50),
    PRIMARY KEY (account_id)
);

DROP TABLE IF EXISTS "Ledger";
CREATE TABLE "Ledger"
(
    id bigserial,
    from_id bigint,
    to_id bigint,
    fee bigint,
    amount bigint,
    date_time timestamp without time zone,
    PRIMARY KEY (id)
);

INSERT INTO accounts(name, credit, bank_name) VALUES ('Jeffery Ford', 1000, 'SpearBank');
INSERT INTO accounts(name, credit, bank_name) VALUES ('Matthew Miller', 1000, 'Tinkoff');
INSERT INTO accounts(name, credit, bank_name) VALUES ('Michelle Ross', 1000, 'SpearBank');
INSERT INTO accounts(name, credit, bank_name) VALUES ('Mr. Bank', 1000, NULL);

COMMIT;


BEGIN;
	SAVEPOINT SP1;
	INSERT INTO "Ledger"(from_id, to_id, fee, amount, date_time) VALUES (1, 3, 0, 500, NOW());
	UPDATE accounts SET credit = credit + 500 WHERE account_id = 3;
	UPDATE accounts SET credit = credit - 500 WHERE account_id = 1;
	
	SAVEPOINT SP2;
	INSERT INTO "Ledger"(from_id, to_id, fee, amount, date_time) VALUES (2, 1, 30, 700, NOW());
	UPDATE accounts SET credit = credit + 700 WHERE account_id = 1;
	UPDATE accounts SET credit = credit - 730 WHERE account_id = 2;
	UPDATE accounts SET credit = credit + 30 WHERE account_id = 4;

	SAVEPOINT SP3;
	INSERT INTO "Ledger"(from_id, to_id, fee, amount, date_time) VALUES (2, 3, 30, 100, NOW());
	UPDATE accounts SET credit = credit + 100 WHERE account_id = 3;
	UPDATE accounts SET credit = credit - 130 WHERE account_id = 2;
	UPDATE accounts SET credit = credit + 30 WHERE account_id = 4;
	
	ROLLBACK TO SAVEPOINT SP2;
ROLLBACK;